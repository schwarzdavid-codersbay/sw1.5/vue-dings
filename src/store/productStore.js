import { defineStore } from 'pinia'

export const useProductStore = defineStore('products', {
  state: () => ({products: []}),
  getters: {
    productsByCategory() {
      return this.products.reduce((output, product) => {
        if(!Object.hasOwn(output, product.category)) {
          output[product.category] = []
        }
        output[product.category].push(product)
        // {
        //   obst: [
        //     {
        //       name: 'apfel',
        //       category: 'obst'
        //     },
        //     { name: 'birne', category: 'obst'}
        //   ],
        //   gemuese: [...],
        //   milch: [{name: 'hafermilch', category: 'milch', isVegan: true}]
        // }
        return output
      }, {}) // 🤡🤡🤡🤡
    }
  }
})
